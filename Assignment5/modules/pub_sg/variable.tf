variable "sg_pb" {
  type        = string
  description = "public security group"
  default     = "-public-sg"
}
variable "https_port" {
  type        = string
  description = "TCP port for https"
  default     = "443"
}
variable "http_port" {
  type        = string
  description = "TCP port for https"
  default     = "80"
}

variable "ssh_port" {
  type        = string
  description = "TCP port for ssh"
  default     = "22"
}
variable "Sonar_dashboard_port" {
  type        = string
  description = "TCP port for sonarqube dashboard"
  default     = "9000"
}
variable "pb_sg_tags" {
  default     = {}
  description = "Public Security group tags for sonar"
  type        = map(string)
}
variable "vpc_id" {
  default     = ""
  type        = string
  description = "VPC ID for sonar vpc"
}


