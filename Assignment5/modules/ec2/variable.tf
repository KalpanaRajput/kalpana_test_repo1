variable "ami" {
  type        = string
  default     = ""
}
variable "ec2_tags" {
  type        = map(string)
  default = {
    Name    = "Sonar-ec2",
    Owner   = "Kalpana",
  }
}
variable "security_group" {
  type        = string
  default     = ""
}
variable "subnet" {
  type        = string
  default     = ""
}

