resource "aws_security_group" "private_security_group" {
  name        = var.sg_pvt
  description = "security group of os engine"
  vpc_id      = var.vpc_id

  ingress {
    description = "Traffic from VPC"
    from_port   = var.https_port
    to_port     = var.https_port
    protocol    = "tcp"
    cidr_blocks = [var.cidr]
  }
  ingress {
    description = "Traffic from VPC"
    from_port   = var.ssh_port
    to_port     = var.ssh_port
    protocol    = "tcp"
    cidr_blocks = [var.cidr]
  }
  ingress {
    description = "port for os-dashboard "
    from_port   = var.Sonar_dashboard_port
    to_port     = var.Sonar_dashboard_port
    protocol    = "tcp"
    cidr_blocks = [var.cidr]
  }
}
