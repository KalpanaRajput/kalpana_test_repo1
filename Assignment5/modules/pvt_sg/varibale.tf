variable "vpc_id" {
  default     = ""
  type        = string
  description = "VPC ID for sonar vpc"
}
variable "sg_pvt" {
  type        = string
  description = "private security group"
  default     = "pvt-sg"
}
variable "https_port" {
  type        = string
  description = "TCP port for https"
  default     = "443"
}
variable "ssh_port" {
  type        = string
  description = "TCP port for ssh"
  default     = "22"
}
variable "Sonar_dashboard_port" {
  type        = string
  description = "TCP port for Sonar dashboard"
  default     = "9000"
}
variable "pvt_sg_tags" {
  default     = {}
  description = "Prvate Security group tags for sonar"
  type        = map(string)
}
variable "cidr" {
  default     = {}
}


