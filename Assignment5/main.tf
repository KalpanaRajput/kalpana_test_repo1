module "my_vpc" {
  source   = "./modules/vpc"
  vpc_cidr   = var.vpc_cidr
  vpc_tags = var.vpc_tags
}
module "IGW_network" {
  source   = "./modules/igw"
  vpc_id   = module.my_vpc.vpc-id
  igw_tags = var.igw_tags
}


module "pub_subnet" {
  source               = "./modules/public_subnet"
  vpc_id               = module.my_vpc.vpc-id
  public_subnet_cidr   = var.public_subnet_cidr
  public_subnet_tags   = var.public_subnet_tags
  public_subnet_zone   = var.public_subnet_zone
  public_routeTable_id = module.pub_route_table.public_routeTable_id
}
module "Nat_traffic" {
  source             = "./modules/nat"
  public_subnet_id = module.pub_subnet.public-subnet-id
  NAT_tags           = var.NAT_tags
}

module "pub_route_table" {
  source                 = "./modules/pub_routetable"
  vpc_id                 = module.my_vpc.vpc-id
  igw_id                 = module.IGW_network.igw-id
  public_routeTable_tags = var.public_routeTable_tags
}
module "pvt_subnet1" {
  source                = "./modules/private_subnet"
  vpc_id                = module.my_vpc.vpc-id
  private_subnet_tags   = var.private_subnet1_tags
  private_subnet_cidr   = var.private_subnet1_cidr
  private_subnet_zone   = var.private_subnet1_zone
  private_routeTable_id = module.pvt_route_table.private_routeTable_id
}
module "pvt_subnet2" {
  source                = "./modules/private_subnet"
  vpc_id                = module.my_vpc.vpc-id
  private_subnet_tags   = var.private_subnet2_tags
  private_subnet_cidr   = var.private_subnet2_cidr
  private_subnet_zone   = var.private_subnet1_zone
  private_routeTable_id = module.pvt_route_table.private_routeTable_id
}
module "pvt_route_table" {
  source                  = "./modules/pvt_routetable"
  vpc_id                  = module.my_vpc.vpc-id
  nat-id                  = module.Nat_traffic.nat-id
  private_routeTable_tags = var.private_routeTable_tags
}

module "pvt_sg" {
  source      = "./modules/pvt_sg"
  sg_pvt      = var.sg_pvt
  vpc_id      = module.my_vpc.vpc-id
  cidr        = var.vpc_cidr
  pvt_sg_tags = var.pvt_sg_tags
}
module "pb_sg" {
  source     = "./modules/pub_sg"
  sg_pb      = var.sg_pb
  vpc_id     = module.my_vpc.vpc-id
   pb_sg_tags = var.pb_sg_tags
}

module "master" {
  source     = "./modules/ec2"
  ami        = var.ami
  subnet = module.pub_subnet.public-subnet-id
  security_group = module.pb_sg.public-sg-id
  ec2_tags = var.master_tag
  }
module "worker1" {
  source     = "./modules/ec2"
  ami        = var.ami
  subnet = module.pvt_subnet1.private-subnet-id
  security_group = module.pvt_sg.private-sg-id
  ec2_tags = var.worker1_tag
}
module "worker2" {
  source     = "./modules/ec2"
  ami        = var.ami
  subnet = module.pvt_subnet2.private-subnet-id
  security_group = module.pvt_sg.private-sg-id
  ec2_tags = var.worker2_tag
  }





