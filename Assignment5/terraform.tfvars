vpc_cidr = "11.0.0.0/16"
vpc_tags = {
  Name    = "My-vpc",
  Owner   = "Kalpana",
  purpose = "sonarqube infra "
}
public_subnet_cidr = "11.0.192.0/19"
public_subnet_tags = {
  Name = "Sonar_pub_subnet"
  Owner = "kalpana"
}
private_subnet1_cidr = "11.0.160.0/19"
private_subnet1_tags = {
  Name = "Sonar_pvt_subnet1"
  Owner = "kalpana"  
}
private_subnet2_cidr = "11.0.128.0/19"
private_subnet2_tags = {
   Name = "Sonar_pvt_subnet2"
   Owner = "kalpana"
}
pvt_sg_tags = {
  Name    = "private-sg"
}
pb_sg_tags = {
  Name    = "public-sg"
}
public_subnet_zone   = "ap-southeast-1a"
private_subnet1_zone = "ap-southeast-1b"
private_subnet2_zone = "ap-southeast-1c"
ami= "ami-07651f0c4c315a529"
master_tag = {
    Name    = "Sonar-Master",
    Owner   = "kalpana",
      }
worker1_tag =  {   
    Name    = "Sonar-worker1",
    Owner   = "Kalpana",
    }
worker2_tag =  {   
    Name    = "Sonar-worker2",
    Owner   = "Kalpana",
    }

   
