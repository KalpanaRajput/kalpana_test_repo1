variable "vpc_id" {
  default     = ""
  type        = string
  description = "VPC ID for Sonarqube vpc"
}
variable "vpc_cidr" {
  default     = ""
  }
variable "vpc_tags" {
  default     = {}
  type        = map(string)
  description = "tags for Sonarqube vpc"
}
variable "public_subnet_cidr" {
  type        = string
  description = "CIDR block for subnet"
  default     = ""
}
variable "public_subnet_tags" {
  default     = {}
  description = "tags for Sonarqube subnet"
  type        = map(string)
}
variable "public_subnet_zone" {
  default = {}
}
variable "private_subnet1_cidr" {
  type        = string
  description = "CIDR block for subnet1"
  default     = ""
}
variable "private_subnet1_tags" {
  default     = {}
  description = "tags for Sonarqubesubnet1"
  type        = map(string)
}
variable "private_subnet2_cidr" {
  type        = string
  description = "CIDR block for subnet2"
  default     = ""
}
variable "private_subnet2_tags" {
  default     = {}
  description = "tags for Sonarqube subnet2"
  type        = map(string)
}
variable "private_subnet1_zone" {
  default = {}
}
variable "private_subnet2_zone" {
  default = {}
}
variable "sg_pvt" {
  type        = string
  description = "private security group"
  default     = "Sonar-pvt-sg"
}
variable "sg_pb" {
  type        = string
  description = "public security group"
  default     = "Sonar-public-sg"
}
variable "https_port" {
  type        = string
  description = "TCP port for https"
  default     = "443"
}
variable "ssh_port" {
  type        = string
  description = "TCP port for ssh"
  default     = "22"
}
variable "Sonar_dashboard_port" {
  type        = string
  description = "TCP port for Sonar dashboard"
  default     = "9000"
}
variable "pvt_sg_tags" {
  default     = {}
  description = "Prvate Security group tags for Sonar"
  type        = map(string)
}
variable "pb_sg_tags" {
  default     = {}
  description = "Public Security group tags for Sonar"
  type        = map(string)
}
variable "private_routeTable_tags" {
  description = "tags for Sonar pvt_rtTable vpc"
  default = {
    Name    = "pvt_RT",
    Owner   = "Kalpana",
    purpose = "sonar_pvt_RT"

  }
}
variable "public_routeTable_tags" {
  description = "tags for Sonar vpc"
  default = {
    Name    = "pub_RT",
    Owner   = "Kalpana",
    purpose = "sonar__pub_RT "

  }
}
variable "NAT_tags" {
  description = "nat-tags for Sonar vpc"
  default = {
    Name    = "sonar-nat",
    Owner   = "kalpana",
    purpose = "sonar-nat"
  }
}
variable "igw_tags" {
  description = "tag for internet gateway of Sonar-vpc"
  default = {
    Name  = "sonar-igw"
    Owner = "kalpana"
  }
}
variable "ami" {
  type        = string
  description = "ami id for sonarqube instances"
  default     = ""
}
variable "master_tag" {
  type        = map(string)
  description = "tags for sonarqube master node instance"
  default = {}
}
variable "worker1_tag" {
  type        = map(string)
  description = "tags for sonarqube worker1 instance"
  default = {}
}
variable "worker2_tag" {
  type        = map(string)
  description = "tags for sonarqube worker2 instance"
  default = {}
}




